na.input = (title, content, successCallback, failCallback, enterOK, settings) => {na.alerts.push (new na.Input(title, content, successCallback, failCallback, enterOK, settings, this));};

na.Input = function (title, content, successCallback, failCallback, enterOK, settings, ctx){
	this.id = na.alerts.length;
	this.zindex = this.id * 3 + 100;
	na.makebase("alert-all alert-alert", this);
	this.titlebar.innerHTML = title
	this.mainbody.innerHTML=content;
	this.textinput = document.createElement("input");
	this.bottombar.appendChild(this.textinput);
	this.textinput.setAttribute("type", "text");
	this.textinput.setAttribute("value","");
	this.textinput.setAttribute("class", "alert-all alert-alert textinput");
	if (enterOK == true || enterOK == undefined){
		this.textinput.addEventListener("keydown", e => {
			if (e.key == "Enter"){
				try{
					successCallback.bind(ctx)(this.textinput.value);
				} finally {
					this.container.parentNode.removeChild(this.container);
				}
			};
		});
	}
	this.failButton = document.createElement("input");
	this.bottombar.appendChild(this.failButton);
	this.failButton.setAttribute("type", "button");
	this.failButton.setAttribute("value","Cancel");
	this.failButton.setAttribute("class", "alert-all alert-alert button");
	this.failButton.addEventListener("click", ()=>{
		try{
			failCallback.bind(ctx)();
		} finally {
			this.container.parentNode.removeChild(this.container);
		}
	});
	this.successButton = document.createElement("input");
	this.bottombar.appendChild(this.successButton);
	this.successButton.setAttribute("type", "button");
	this.successButton.setAttribute("value","OK");
	this.successButton.setAttribute("class", "alert-all alert-alert button");
	this.successButton.addEventListener("click", ()=>{
		try {
			successCallback.bind(ctx)(this.textinput.value);
		} finally {
			this.container.parentNode.removeChild(this.container);
		}
	});
};
