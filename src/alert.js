na.alert = (title, content, callback, settings) => {na.alerts.push (new na.Alert(title, content, callback, settings, this));};

na.Alert = function (title, content, callback, settings, ctx){
	this.id = na.alerts.length;
	this.zindex = this.id * 3 + 100;
	na.makebase("alert-all alert-alert", this);
	this.titlebar.innerHTML = title
	this.mainbody.innerHTML=content;
	this.button = document.createElement("input");
	this.bottombar.appendChild(this.button);
	this.button.setAttribute("type", "button");
	this.button.setAttribute("value","OK");
	this.button.setAttribute("class", "alert-all alert-alert button");
	this.button.addEventListener("click", ()=>{
		try{
			callback.bind(ctx)();
		} finally {
			this.container.parentNode.removeChild(this.container);
		}
	});
};
