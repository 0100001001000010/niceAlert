var na = {
	settings: {
		x: 0,
		y: 0,
		timeout: 0
	},
	makebase: (classes, ctx) => {
		ctx.container = document.createElement("div");
		ctx.container.setAttribute("class", classes);
		ctx.container.style.zindex = ctx.zindex;
		document.body.appendChild(ctx.container);
		ctx.titlebar = document.createElement("div");
		ctx.titlebar.setAttribute("class", classes + " titlebar");
		ctx.titlebar.style.zindex = ctx.zindex + 2;
		ctx.container.appendChild(ctx.titlebar);
		ctx.mainbody = document.createElement("div");
		ctx.mainbody.setAttribute("class", classes + " mainbody");
		ctx.mainbody.style.zindex = ctx.zindex + 1;
		ctx.container.appendChild(ctx.mainbody);
		ctx.bottombar = document.createElement("div");
		ctx.bottombar.setAttribute("class", classes + " bottombar");
		ctx.bottombar.style.zindex = ctx.zindex + 2;
		ctx.container.appendChild(ctx.bottombar);
	},
	alerts: []
};
