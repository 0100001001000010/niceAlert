# Release 1.0.0-nodocs


## New stuff
This is the first release of this library.

## Bug fixes
There were no previous releases, so no bugs have been fixed

## Files
* niceAlert-1.0.0.js: 4524 bytes: The entire library source code refactored into one file
* niceAlert-1.0.0.min.js: 3923 bytes: Same as the file above, but minified
* styles.css: 917 bytes: A basic stylesheet that makes the library usable. You probably will not like it, so please customize it.
* styles.min.css: 751 bytes: Same as the file above, but minified

## Total sizes
These calculations assume no modifications to the code.
* All non-minified files: 5441 bytes
* All minified files: 4674 bytes
* Space saved by using minified version: 767 bytes,

## Other notes
The documentation is not yet ready. At the time of the release, absolutely none has been written. The code is pretty small and the variables are self explanatory. A new release will be made when the documentation is ready
