# Release 1.0.0


## New stuff
The documentation had been written.

## Bug fixes
None.

## Files
* niceAlert-1.0.0.js: 4524 bytes: The entire library source code refactored into one file
* niceAlert-1.0.0.min.js: 3923 bytes: Same as the file above, but minified
* styles.css: 917 bytes: A basic stylesheet that makes the library usable. You probably will not like it, so please customize it.
* styles.min.css: 751 bytes: Same as the file above, but minified

## Total sizes
These calculations assume no modifications to the code.
* All non-minified files: 5441 bytes
* All minified files: 4674 bytes
* Space saved by using minified version: 767 bytes,

## Other notes
In the source, the file setup.js has been renamed core.js to make its functionality more obvious. All development files have been moved into the /src folder to avoid mixups with the metadata and configuration files. Neither of these changes affect the final product, only the development and testing process.
