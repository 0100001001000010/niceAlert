# niceAlert

A nice, modern, and customizable form of JavaScript alert()s.

## Introduction

JavaScript alerts are nice for development. They are very easy to use (just one line of code) and pretty easy for users to deal with. However, JavaScript alerts have problems. This does not aim to be a complete list, but here are some that I noticed:

*   They always look the same, no matter what site you are on. On some sites, they might look good. On others, they will look bad because they will not look like the rest of your UI. They look really bad on my terminal-themed website:  
    ![](docs/bad-alert.png)  
    Alerts provided by NiceAlert will be customizable to match your UI.
*   They are synchronous in every implimentation I have seen. This means that all JavaScript code on your page pauses when your alert is displayed. This may not be what you want. You might want other things to continue to happen. NiceAlert adds alerts to the DOM and uses callbacks to allow you to respond to button presses. You can do other things while your alert is displayed. Plus, you can display multiple alerts at once.
*   You can only put plain text in every implimentation of JavaScript alerts that I have seen. Because you are passing text to niceAlert and it is being put into the DOM with innerHTML, you can put anything in here. Links, images, videos, scripts, games, etc.

NiceAlert aims to solve these problems. More documentation is available in HTML format in the /docs folder on the master branch. View it [in the repo](https://github.com/0100001001000010/niceAlert/tree/master/docs) and [on GitHub Pages](https://bruceblore.me/niceAlert/documentation.html).

## Installation

This library is easy to use. It has no dependencies, so you can load it at some point before your JavaScript loads. Just add a script tag with the minified file. You can host it on your server if you would like to reduce DNS lookups

    <script src="niceAlert.min.js"></script>

## Building

The minified files I provide have the full functionality. You may build one yourself if you do not need all of it. Currently, the build process is manual and involves me copy-pasting the files together and minifying with find and replace. I am considering trying WebPack.

## Versioning

SemVer is used for versioning. Major releases are incompatible with older releases, minor releases are compatible, patches are bug fixes. A tag will be added, the documentation will be updated, and a minified file will be put in the [/releases](https://githun.com/0100001001000010/niceAlert/tree/master/releases) folder for each release. I can't stop you from using an in-development version, but those might (will) be buggy and you will have to build and minify them yourself.

## Contributing

Determine whether you are submitting a bug fix or a new feature.

### Bug fix

1.  Run the latest development version to see if it is fixed already.
2.  If not, send a pull request against the applicable branch (almost always master) with your bug fix.

### New feature

1.  See if it is in development already. If so, help with it instead of doing duplicate work.
2.  Fork the repository and create a new branch for your feature. Make sure to regularly pull in changes from upstream while you do your work.
3.  When you are done, send me the pull request

## Authors

This JavaScript library was developed by me. If anyone else contributes, I will try to name them here.</div>
